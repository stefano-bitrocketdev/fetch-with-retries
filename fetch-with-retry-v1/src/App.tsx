import React, { useState } from "react"
import logo from "./logo.svg"
import "./App.css"
import { getData } from "./api"

const App: React.FC = () => {
  const [callState, setCallState] = useState<{ loading: boolean; error?: any; data?: any }>({ loading: false })

  const handleOnClick = async () => {
    setCallState({ loading: true })

    try {
      const data = await getData()
      setCallState({ loading: false, data })
    } catch (error) {
      console.error(error)
      setCallState({ ...callState, loading: false, error })
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>To test your app, turn off your internet connection (or disable through Chrome DevTool) and click the button</p>
        <button onClick={handleOnClick} disabled={callState.loading}>
          CLICK ME TO FETCH DATA
        </button>
        <br />
        {callState.loading ? (
          <p>Loading data . . .</p>
        ) : callState.error ? (
          <b style={{ color: "red" }}>
            OPS! Something went wrong! <i>{callState.error}</i>
          </b>
        ) : !!callState.data ? (
          <p>{JSON.stringify(callState.data)}</p>
        ) : (
          "Nothing to show"
        )}
      </header>
    </div>
  )
}

export default App
