import { fetchWithRetries } from "./utils"

export const getData = () => fetchWithRetries("https://jsonplaceholder.typicode.com/posts", undefined, 3, 1500)
